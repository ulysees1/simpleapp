const express = require('express');
const app = express();
const port = 3402;

app.get('/', (req, res) => {
    res.send("This is a Simple Application");
});


app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
