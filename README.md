# SimpleApp



## Create Gitlab repository

- Create a Gitlab repository named SimpleApp.
- Create a simple application app.js using express module which will perform a simple task.
- Create a package.json file for express module dependency.
- Push the application files to the Gitlab repository.

## Jenkins Setup

- Create a new Jenkins pipeline project and define a pipeline script from SCM by providing the Gitlab project url.
- In Build Triggers check trigger on new commit
- Create a new Jenkinsfile in the Gitlab project and add stages for checkout, build, test and deploy.

## Gitlab CI configuration

- Create a .gitlab-ci.yml file and provide command for triggering a Jenkins buils for each commit to the repository.
